#Claims management#

A claims management portal for processing the reimbursement documents

## Installation

1) Clone the repository

2) Create a virtualenv and activate with 
```
#!shell

virtualenv claims_management_env
source claims_management_env/bin/activate
```
3) Change to working directory and install requirements 
```
#!shell
 
pip install -r requirements.txt
```
4) Apply the migrations
```
#!shell
 
python manage.py migrate
```
5) Load some initial data
```
#!shell
 
python manage.py loaddata fixtures.json
```

## Testing

Run tests with 
```
#!python

python manage.py test
```