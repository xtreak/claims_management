from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.authentication import SessionAuthentication
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser

from django.http import Http404
from django.contrib.auth import authenticate, login, logout

from .models import *
from .serializers import *
from .utils import save_file


@api_view(['POST'])
def signup(request):
    password = request.POST.get("password", "")
    username = request.POST.get("username", "")

    user, created = User.objects.get_or_create(
        username=username, password=password)
    if created:
        employee = Employee.objects.create(user=user)
        return Response({"success": "Successfully created"}, status=status.HTTP_200_OK)
    else:
        return Response({"error": "Username already exists"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def signout(request):
    logout(request)
    return Response({"success": "Successful logout"}, status=status.HTTP_200_OK)


class TeamListView(APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        teams = Team.objects.all()
        serializer = TeamSerializer(teams, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = TeamSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TeamDetailView(APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, request, pk, format=None):
        team = Team.objects.get(id=pk)
        serializer = TeamSerializer(team)
        return Response(serializer.data)


class ClaimListView(APIView):

    permission_classes = (IsAuthenticated, )
    parser_classes = (FormParser, MultiPartParser, )

    def get(self, request, format=None):
        user = request.user
        team = Team.objects.filter(financial_manager=user)
        if team:
            # Get only claims in new state for the FM and the FM might also be an employee
            # TODO : Move to bette endpoint
            fm_claims = Claim.objects.filter(
                team=team).exclude(state=Claim.STATE_NEW)
            user_claims = Claim.objects.filter(user=user)
            claims = fm_claims | user_claims
        else:
            claims = Claim.objects.filter(user=user)
        serializer = ClaimGetSerializer(claims, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        user = request.user
        file = request.FILES.get('invoice')
        if not file:
            return Response({"error": "Invoice not found"}, status=status.HTTP_400_BAD_REQUEST)

        employee = Employee.objects.get(user=user)
        team = Team.objects.get(employees=employee)
        serializer = ClaimPostSerializer(data=request.data)
        if serializer.is_valid():
            claim = serializer.save(team=team, user=user)
            claim_id = str(claim.id)
            user_id = str(user.id)
            save_file(file, claim_id, user_id)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ClaimDetailView(APIView):

    permission_classes = (IsAuthenticated,)

    def get_object(self, pk, user=None, team=None):
        try:
            if user:
                return Claim.objects.get(id=pk, user=user)
            elif team:
                return Claim.objects.get(id=pk, team=team)
        except Claim.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        user = request.user
        team = Team.objects.filter(financial_manager=user)
        if team:
            claim = self.get_object(pk, team=team)
        else:
            claim = self.get_object(pk, user=user)
        serializer = ClaimGetSerializer(claim)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        user = request.user
        team = Team.objects.filter(financial_manager=user)
        if team:
            claim = self.get_object(pk, team=team)
        else:
            claim = self.get_object(pk, user=user)
        state = request.POST.get('state', '')

        if team:
            # Handle FM approving new claims
            if claim.state == claim.STATE_NEW:
                return Response({"error": "State cannot be edited since its not sent for approval"}, status=status.HTTP_400_BAD_REQUEST)

            if state == "approve":
                claim.state = claim.STATE_APPROVED
            elif state == "reject":
                claim.state = claim.STATE_REJECTED
            elif state == "settle":
                claim.state = claim.STATE_SETTLED
        elif state == "apply":
            # Make sure user state is new so that the user can change it
            if claim.state == claim.STATE_NEW:
                claim.state = claim.STATE_SENT_TO_APPROVAL
            else:
                return Response({"error": "State cannot be edited"}, status=status.HTTP_400_BAD_REQUEST)
        elif state:
            return Response({"error": "Unknown state"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"error": "state not found"}, status=status.HTTP_400_BAD_REQUEST)

        claim.save()
        return Response({"success": "state changed to " + state}, status=status.HTTP_200_OK)
