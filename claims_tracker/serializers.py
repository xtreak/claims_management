from rest_framework import serializers
from .models import *


class EmployeeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Employee
        fields = ('user')


class ClaimGetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Claim
        fields = ('id', 'team', 'user', 'date',  'description', 'state')


class ClaimPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Claim
        fields = ('id', 'date', 'description', 'state')


class TeamSerializer(serializers.ModelSerializer):

    class Meta:
        model = Team
        fields = ('id', 'name', 'financial_manager', 'employees')
