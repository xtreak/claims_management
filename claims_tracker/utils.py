from django.conf import settings
import os


def save_file(file, claim_id, user_id):
    '''
    file - File object
    claim_id - Claim id for the object
    user_id - user_id

    Saves the file in the location of the form <claim_id>_<user_id>_<file_name>
    '''
    name = file._name
    filename = '_'.join([claim_id, user_id, name])
    FILE_STORAGE_PATH = settings.FILE_STORAGE_PATH
    dest_path = os.path.join(FILE_STORAGE_PATH + filename)

    with open(dest_path, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
