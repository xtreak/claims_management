from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
import claims_tracker.views as views

urlpatterns = [
    url('^api/signup/$', views.signup, name="api-signup"),
    url('^api/signout/$', views.signout, name="api-signin"),
    url('^api/teams/$', views.TeamListView.as_view(), name="api-team-list"),
    url('^api/teams/(?P<pk>[0-9]+)$',
        views.TeamDetailView.as_view(), name="api-team-detail"),
    url('^api/claims/$', views.ClaimListView.as_view(), name="api-claim-list"),
    url('^api/claims/(?P<pk>[0-9]+)$',
        views.ClaimDetailView.as_view(), name="api-claim-detail"),
]
