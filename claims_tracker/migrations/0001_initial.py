# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Claim',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('description', models.CharField(max_length=100)),
                ('invoice', models.FileField(upload_to='')),
                ('state', models.IntegerField(default=0, choices=[
                 (0, 'new'), (1, 'sent_to_approval'), (2, 'successful'), (3, 'failed'), (4, 'settled')])),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('employees', models.ManyToManyField(
                    to='claims_tracker.Employee')),
                ('financial_manager', models.ForeignKey(
                    to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='claim',
            name='team',
            field=models.ForeignKey(to='claims_tracker.Team'),
        ),
        migrations.AddField(
            model_name='claim',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
