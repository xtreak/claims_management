from django.contrib.auth.models import User
from django.db import models


class Employee(models.Model):
    user = models.ForeignKey(User)


class Team(models.Model):
    name = models.CharField(max_length=100)
    financial_manager = models.ForeignKey(User)
    employees = models.ManyToManyField(Employee)


class Claim(models.Model):

    STATE_NEW = 0
    STATE_SENT_TO_APPROVAL = 1
    STATE_APPROVED = 2
    STATE_REJECTED = 3
    STATE_SETTLED = 4

    STATE_CHOICES = (
        (STATE_NEW, 'new'),
        (STATE_SENT_TO_APPROVAL, 'sent_to_approval'),
        (STATE_APPROVED, 'successful'),
        (STATE_REJECTED, 'failed'),
        (STATE_SETTLED, 'settled'),
    )

    team = models.ForeignKey(Team)
    user = models.ForeignKey(User)
    date = models.DateField()
    description = models.CharField(max_length=100)
    invoice = models.FileField()
    state = models.IntegerField(choices=STATE_CHOICES, default=0)
