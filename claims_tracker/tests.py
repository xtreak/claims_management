from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIClient

import datetime

from .models import *


class APITestCase(APITestCase):
    fixtures = ['fixtures.json']

    def setUp(self):
        self.fm_username = 'a'
        self.fm_password = 'example'
        self.fm_client = APIClient()
        self.fm_client.login(username=self.fm_username,
                             password=self.fm_password)
        self.b_username = "b"
        self.b_password = "eb"
        self.b_client = APIClient()
        self.b_client.login(username=self.b_username, password=self.b_password)
        self.c_username = "c"
        self.c_password = "c"
        self.c_client = APIClient()
        self.c_client.login(username=self.c_username, password=self.c_password)

    def test_get_user_specific_new_claims(self):
        url = reverse('api-claim-list')
        response = self.c_client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 8)

    def test_get_user_apply_claims(self):
        url = reverse('api-claim-detail', kwargs={'pk': 13})
        response = self.c_client.get(url)
        self.assertEqual(response.data['state'], Claim.STATE_NEW)

        response = self.c_client.put(url, {'state': 'apply'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api-claim-detail', kwargs={'pk': 13})
        response = self.c_client.get(url)
        self.assertEqual(response.data['state'], Claim.STATE_SENT_TO_APPROVAL)

    def test_get_fm_specific_claims(self):
        url = reverse('api-claim-list')
        response = self.fm_client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_create_claims(self):
        url = reverse('api-claim-list')
        response = self.c_client.get(url)
        initial_count = len(response.data)

        url = reverse('api-claim-list')

        with open('manage.txt') as invoice:
            response = self.c_client.post(
                '/api/claims/', {'date': '2016-01-01', 'description': 'Test description', 'invoice': invoice}, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        url = reverse('api-claim-list')
        response = self.c_client.get(url)
        self.assertEqual(len(response.data), initial_count + 1)
